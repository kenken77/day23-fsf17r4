(function(){
    angular
        .module("PopQuizMongodbApp")
        .service("PopQuizAppAPI", [
            '$http',
            PopQuizAppAPI
        ]);
    
    function PopQuizAppAPI($http){
        var self = this;
        self.list = function(){
            return $http.get("/api/v1/popquizes");
        }
    }
})();