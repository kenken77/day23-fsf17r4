(function () {
    angular
        .module("PopQuizMongodbApp")
        .controller("PopQuizCtrl", ['PopQuizAppAPI', PopQuizCtrl])
    
    function PopQuizCtrl(PopQuizAppAPI){
        var self = this;
        self.triggerText = null;
        console.log("PopQuizCtrl");
        PopQuizAppAPI.list().then((result)=>{
            self.popquizes = result.data;
        });
    }
})();