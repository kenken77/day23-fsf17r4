var express = require("express");
var bodyParser = require('body-parser');

var config = require("./config");

var app = express();
var oneYear = 31557600000;

var initializeDatabases = require('./mongodb');

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

app.use(express.static(__dirname + "/../client/", { maxAge: oneYear }));

initializeDatabases(function(err, mongodb) {
    if (err) {
      console.error('Failed to make all database connections!');
      console.error(err);
      process.exit(1);
    }
    require("./routes")(app,mongodb);

    app.listen(config.port, function () {
        console.log(`Server running at port ${config.port}`);
    });
});