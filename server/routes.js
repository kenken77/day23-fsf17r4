'use strict';

module.exports = function(app, mongodb) {
    app.get("/api/v1/popquizes", (req, res)=>{ 
        var popquizes = mongodb.production.collection("popquizes");
        var query = {};
        popquizes.find(query).toArray(function(err, quizes){
            console.log(quizes);
            res.status(200).json(quizes);     
        });
    });
}